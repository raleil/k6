import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;


/**
 * Sentence generator.
 * This program can generate sentences starting and ending with the specified words.
 * You must provide some text as an input before its possible to generate any sentence.
 * The more text you feed into this program the better the sentences are.
 * A file with more than 50000 words is recommended for its use.
 */
public class GraphTask {

   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /**
    * Run word Graph examples.
    */
   public void run() {
      // Example 1
      System.out.println("EXAMPLE 1");
      String text = "a b c d e. c d b. c e.";
      Graph wordGraph = createWordGraph(text);
      System.out.println(wordGraph);

      // From c to e
      String firstWord = "c";
      String lastWord = "e";
      LinkedList<Vertex> route = wordGraph.findRoute(firstWord, lastWord, 3);
      System.out.println("Route from " + firstWord + " to " + lastWord);
      System.out.println("Route: " + getRouteString(route));
      System.out.println("Sentence: " + getSentenceString(route));

      // Example 2
      // Text from: https://www.lipsum.com/
      System.out.println("\n\nEXAMPLE 2");
      text = "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?";
      wordGraph = createWordGraph(text);

      printPossibleSentences(wordGraph, "i", "idea", 10);

      route = wordGraph.findRoute("to", "all", 4);
      System.out.println("\n");
      System.out.println(getRouteString(route));
      System.out.println(getSentenceString(route));

      // First word doesn't exist.
      printPossibleSentences(wordGraph, "hello", "nowhere", 1);

      // Last word doesn't exist.
      printPossibleSentences(wordGraph, "i", "nowhere", 1);

      // Example 3
      // Text from: https://www.lipsum.com/
      System.out.println("\n\nEXAMPLE 3");
      text = "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.";
      wordGraph = createWordGraph(text);

      printPossibleSentences(wordGraph, "we", "are", 10);

      // Example 4
      // Text from text.txt file
      // The file is used to create larger graphs, since using just a String variable is limited.
      // Uncomment these lines if you want to use a file for text input.
      /*
      System.out.println("\n\nEXAMPLE 4");
      text = readTextFromFile("text.txt");
      wordGraph = createWordGraph(text);

      printPossibleSentences(wordGraph, "hello", "world", 10);
       */
   }

   /**
    * Get an informative {@code String} about the route.
    * @param route  {@code LinkedList<Vertex} containing vertices that are linked with one another.
    * @return  Informative {@code String} about the route.
    */
   public static String getRouteString(LinkedList<Vertex> route) {
      StringBuilder sb = new StringBuilder();
      LinkedList<Vertex> words = new LinkedList<>(route);

      if (route.isEmpty()) {
         return sb.toString();
      }

      Vertex lastWord = words.pop();
      sb.append(lastWord.id);

      while (!words.isEmpty()) {
         Vertex word = words.pop();

         Arc arc = lastWord.findArcById(lastWord.id + word.id);

         if (arc != null) {
            sb.append(" -(").append(arc.weight).append(")> ");
         }

         sb.append(word.id);

         lastWord = word;
      }

      return sb.toString();
   }


   /**
    * Makes a sentence out of the provided route.
    * @param route  {@code LinkedList<Vertex} containing vertices that are linked with one another.
    * @return  A string resembling a sentence.
    */
   public static String getSentenceString(LinkedList<Vertex> route) {
      StringBuilder sentence = new StringBuilder();

      for (Vertex vertex : route) {
         sentence.append(vertex.id).append(" ");
      }

      return sentence.toString().trim();
   }


   /**
    * Print out word graph sentences using the first word and the last word.
    * This will print out sentences that are possible with 1 to N words.
    * @param wordGraph  The {@code Graph} to be used for generating the sentences.
    * @param firstWord  First word of the sentence.
    * @param lastWord  Last word of the sentence.
    * @param numOfWords  Number of words to include in the sentence.
    */
   public static void printPossibleSentences(Graph wordGraph, String firstWord, String lastWord, int numOfWords) {
      for (int i = 1; i <= numOfWords; i++) {
         try {
            LinkedList<Vertex> route = wordGraph.findRoute(firstWord, lastWord, i);
            System.out.println("\nSentence with " + i + " words:");
            System.out.println("Route: " + getRouteString(route));
            System.out.println("Sentence: " + getSentenceString(route));
         } catch (RuntimeException error) {
            System.out.println(error.getMessage());
         }
      }
   }

   /**
    * Creates a word graph out of the provided text.
    * @param text  Text which will be used for the word graph.
    * @return  The {@code Graph} containing the words.
    */
   public Graph createWordGraph(String text) {
      Graph wordGraph = new Graph("WordGraph");

      for (String sentence : textToSentences(text)) {
         LinkedList<String> words = textToWords(sentence);

         if (words.isEmpty()) {
            continue;
         }

         Vertex last = wordGraph.addVertex(words.pollFirst());
         while (!words.isEmpty()) {
            String word = words.pollFirst();

            if (last.id.equals(word)) {
               continue;
            }

            Vertex first = wordGraph.addVertex(word);

            wordGraph.addArc(last, first);

            last = first;
         }
      }

      return wordGraph;
   }

   /**
    * Reads the text file and returns a single {@code String} containing all the text from the file.
    * File must be placed in the projects root directory.
    * @param filePath  Path to the file.
    * @return  The files text.
    */
   public static String readTextFromFile(String filePath) {
      StringBuilder stringBuilder = new StringBuilder();

      try {
         File file = new File(filePath);
         Scanner scanner = new Scanner(file);

         while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (! line.trim().isEmpty()) {
               stringBuilder.append("\n").append(line);
            }
         }

      } catch (FileNotFoundException e) {
         e.printStackTrace();
      }

      return stringBuilder.toString();
   }

   /**
    * Converts the text to a list of sentences.
    * This splits the text at the following symbols: ".", "?", "!"
    * Splitting the text into sentences, before extracting the words, provides better results.
    * @param text  Text to split into sentences.
    * @return  List of sentences.
    */
   private static List<String> textToSentences(String text) {
      List<String> sentences = new ArrayList<>();

      text = text.replaceAll("\n", " ");

      for (String sentence : text.split("[.?!]")) {
         sentence = sentence.trim();

         if (!sentence.isEmpty()) {
            sentences.add(sentence);
         }
      }

      return sentences;
   }

   /**
    * Convert the provided text to a list of words.
    * This will strip all the unnecessary symbols and convert the words to lowercase.
    * @param text  To convert into words.
    * @return  {@code LinkedList} of words.
    */
   private static LinkedList<String> textToWords(String text) {
      LinkedList<String> words = new LinkedList<>();

      if (!text.contains(" ")) {
         words.add(text);
         return words;
      }

      for (String word : text.split("[.,:;-]*\\s+")) {
         word = word.trim().replaceAll("[\"”“]", "").toLowerCase();

         if (!word.isEmpty() && word.matches("\\w+")) {
            words.add(word);
         }
      }

      return words;
   }

   /**
    * Graph vertex
    */
   class Vertex {

      private final String id;
      private Vertex next;
      private Arc first;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      /**
       * Finds the {@code Vertex} with the specified id recursively.
       * @param id  The id of the {@code Vertex} to find.
       * @return  The found {@code Vertex} or {@code null} when the {@code Vertex} with that id doesn't exist.
       */
      private Vertex findById(String id) {
         if (this.id.equals(id)) {
            return this;
         }

         if (this.next == null) {
            return null;
         }

         return this.next.findById(id);
      }

      /**
       * Finds the {@code Arc} with the specified id recursively.
       * @param arcId  The id of the {@code Arc} to find.
       * @return  The found {@code Arc} or {@code null} when the {@code Arc} with that id doesn't exist.
       */
      public Arc findArcById(String arcId) {
         if (this.first == null) {
            return null;
         }

         return this.first.findById(arcId);
      }

      /**
       * Sorts this {@code Vertex} arcs in a descending order.
       * @return  This {@code Vertex} arcs in descending order.
       */
      public LinkedList<Arc> getArcsSortedByWeight() {
         LinkedList<Arc> sortedArcs = new LinkedList<>();

         Arc currentArc = first;
         while (currentArc != null) {
            if (sortedArcs.size() == 0) {
               sortedArcs.add(currentArc);
            } else {
               for (int i = 0; i < sortedArcs.size(); i++) {
                  if (sortedArcs.get(i).weight <= currentArc.weight) {
                     sortedArcs.add(i, currentArc);
                     break;
                  }
               }
            }

            currentArc = currentArc.next;
         }

         return sortedArcs;
      }

      @Override
      public String toString() {
         return id;
      }

      @Override
      public boolean equals(Object o) {
         if (!(o instanceof Vertex)) {
            return false;
         }

         Vertex vertex = (Vertex) o;

         return Objects.equals(id, vertex.id)
                 && Objects.equals(next, vertex.next)
                 && Objects.equals(first, vertex.first);
      }

      @Override
      public int hashCode() {
         return Objects.hash(id);
      }
   }

   /**
    * Vertex arc
    */
   class Arc {

      private final String id;
      private Vertex target;
      private Arc next;
      private int weight = 0;

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      /**
       * Finds the {@code Arc} with the specified id recursively.
       * @param id The id of the {@code Arc} to find.
       * @return The found {@code Arc} or {@code null} when the {@code Arc} with that id doesn't exist.
       */
      private Arc findById(String id) {
         if (this.id.equals(id)) {
            return this;
         }

         if (this.next == null) {
            return null;
         }

         return this.next.findById(id);
      }

      @Override
      public String toString() {
         return id + " (" + weight + ")";
      }

      @Override
      public boolean equals(Object o) {
         if (!(o instanceof Arc)) {
            return false;
         }

         Arc arc = (Arc) o;

         return weight == arc.weight
                 && Objects.equals(id, arc.id)
                 && Objects.equals(target, arc.target)
                 && Objects.equals(next, arc.next);
      }

      @Override
      public int hashCode() {
         return Objects.hash(id, weight);
      }
   }

   /**
    * Word graph
    */
   class Graph {

      private final String id;
      private Vertex first;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      /**
       * Finds the {@code Vertex} with the specified id recursively.
       * @param vertexId The id of the {@code Vertex] to find.
       * @return The found {@code Vertex} or {@code null} when the {@code Arc} with that id doesn't exist.
       */
      public Vertex findVertexById(String vertexId) {
         if (first == null) {
            return null;
         }

         return first.findById(vertexId);
      }

      /**
       * Creates a new {@code Vertex} with the specified id on this {@code Graph},
       * if a {@code Vertex} with the id doesn't exist already.
       * @param vertexId id for the new {@code Vertex}.
       * @return Created or found {@code Vertex}.
       */
      public Vertex addVertex(String vertexId) {
         Vertex vertex = findVertexById(vertexId);

         if (vertex == null) {
            vertex = createVertex(vertexId);
         }

         return vertex;
      }

      /**
       * Creates a new {@code Vertex} with the specified id on this {@code Graph}.
       * The new {@code Vertex} gets pushed to the first position.
       * @param vertexId id for the new {@code Vertex}.
       * @return Created {@code Vertex}.
       */
      private Vertex createVertex (String vertexId) {
         Vertex res = new Vertex (vertexId);
         res.next = first;
         first = res;
         return res;
      }

      /**
       * Creates a new {@code Arc} if it doesn't exist already.
       * When an existing {@code Arc} is found, it's weight gets increased by 1.
       * @param from {@code Vertex} where the {@code Arc} originates from.
       * @param to {@code Vertex} which the {@code Arc} points to.
       * @return Created or modified {@code Arc}.
       */
      public Arc addArc(Vertex from, Vertex to) {
         String arcId = from.id + to.id;
         Arc arc = from.findArcById(arcId);

         if (arc != null) {
            arc.weight += 1;

            return arc;
         }

         return createArc(arcId, from, to);
      }

      /**
       * Creates a new {@code Arc} with the specified id.
       * The new {@code Arc} gets pushed to the first position on the {@code Vertex from}.
       * @param aid id for the new {@code Arc}
       * @param from {@code Vertex} where the {@code Arc} originates from.
       * @param to {@code Vertex} which the {@code Arc} points to.
       * @return Created {@code Arc}.
       */
      private Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Breadth-First Search (BFS) algorithm used to gather a list of {@code Vertex}.
       * This will gather the amount of vertices specified in the "steps" parameter,
       * starting from {@code Vertex start} to {@code Vertex end}.
       *
       * Rough inspiration taken from the following snippet of code:
       * https://www.techiedelight.com/find-path-between-vertices-directed-graph/
       * @param start Starting {@code Vertex}. This is where we start searching for the end {@code Vertex}.
       * @param end Final {@code Vertex}.
       * This will be the destination we are trying to reach from the start {@code Vertex}.
       *
       * @param steps  The number of vertices we must gather before reaching the end {@code Vertex}.
       * @param discovered  A {@code HashSet} of the discovered vertices.
       * @param route  {@code LinkedList} of all the vertices we gathered.
       * @return  Whether we found a path leading from the start {@code Vertex} to the end {@code Vertex}.
       */
      private boolean isReachable(
              Vertex start,
              Vertex end,
              int steps,
              HashSet<Vertex> discovered,
              LinkedList<Vertex> route
      ) {
         if (steps <= route.size()) {
            return false;
         }

         discovered.add(start);
         route.add(start);

         if (start == end && steps == route.size()) {
            return true;
         }

         LinkedList<Arc> sortedArcs = start.getArcsSortedByWeight();

         for (Arc arc : sortedArcs) {
            if (!discovered.contains(arc.target)) {
               if (isReachable(arc.target, end, steps, discovered, route)) {
                  return true;
               }
            }
         }

         route.pollLast();

         return false;
      }


      /**
       * Finds route between {@code firstWord} and {@code lastWord}, that is {@code words} long.
       * @param firstWord  A word to use as a start.
       * @param lastWord  A word to use for the end.
       * @param length  Length of the route.
       * @return {@code LinkedList<Vertex>} containing the found route,
       * between the {@code firstWord} and {@code lastWord}.
       */
      public LinkedList<Vertex> findRoute(String firstWord, String lastWord, int length) {
         Vertex startVertex = this.findVertexById(firstWord);

         if (startVertex == null) {
            throw new RuntimeException("Failed to find first word: " + firstWord);
         }

         Vertex endVertex = this.findVertexById(lastWord);

         if (endVertex == null) {
            throw new RuntimeException("Failed to find last word: " + lastWord);
         }

         LinkedList<Vertex> route = new LinkedList<>();

         if (!isReachable(startVertex, endVertex, length, new HashSet<>(), route)) {
            throw new RuntimeException("Could not make a sentence with " + length + " words");
         }

         return route;
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v);
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.id);
               sb.append (" (");
               sb.append (v);
               sb.append (" -(").append(a.weight).append(")> ");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }
   }

} 

